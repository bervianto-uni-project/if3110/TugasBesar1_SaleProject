<!DOCTYPE html>
<html>
	<head>
		<title>Register</title>
    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
		<script type="text/javascript" src="js/validate.js"></script>
	</head>

	<body>
	<?php 
		require 'include/connect.php';

		function register() {
			$connect = connectToDatabase();
			if (!$connect) {
				die('Connection Failed'.mysqli_error());
			} else {
				//Check if data in database
				$queUser = "SELECT username FROM account WHERE account.username='".$_POST['username']."'";
				$testUserName = mysqli_query($connect,$queUser) or die(mysqli_error($connect));
				if (mysqli_num_rows($testUserName)>=1) {
					echo "Username already used.";
					echo "<meta http-equiv='refresh' content='0; URL=./register.php?register=0'>";
				} else {
					$queEmail = "SELECT username FROM account WHERE account.email='".$_POST['email']."'";
					$testEmail = mysqli_query($connect,$queEmail) or die(mysqli_error($connect));
					if (mysqli_num_rows($testEmail)>=1) {
						echo "Your email already used.";
						echo "<meta http-equiv='refresh' content='0; URL=./register.php?register=1'>";
					} else {
						//New Data
						$query = "INSERT into account(fullname,username,email,password,address,postalcode,phonenumber) values('".$_POST['fullname']."','".$_POST['username']."','".$_POST['email']."','".$_POST['password']."','".$_POST['address']."','".$_POST['postalcode']."','".$_POST['phonenumber']."')"; 
						$res = mysqli_query($connect,$query) or die(mysqli_error($connect));  
						if ($res){
							session_start(); //starting the session for user profile page
							$que = "SELECT id FROM account WHERE username='".$_POST['username']."'";
							$result = mysqli_query($connect,$que) or die (mysqli_error($connect));
							if (mysqli_num_rows($result)==1) {
								$id_id = mysqli_fetch_assoc($result);
								$id = $id_id['id'];
								echo "<meta http-equiv='refresh' content='0; URL=./catalog.php?id_active=$id'>"; /* Redirect browser */
							} else {
								echo "<meta http-equiv='refresh' content='0; URL=./register.php'>";
							}
						} else {
							echo "<meta http-equiv='refresh' content='0; URL=./register.php'>";
						}
					}
				}
				mysqli_close($connect);
			}
		}
		if(isset($_POST['register'])) {
			if (!empty($_POST['fullname']) && !empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['email']))
			{
				register();
			}
		}
	?>
	<section class="section">
	<div class="container">
		<div class="box">
			<h1 class="title has-text-centered"> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
			<h2 class="subtitle has-text-centered"> Please register </h2>
			<div class="notif">
			<?php
				if(isset($_GET['register'])) {
					if ($_GET['register']==0) {
						echo "<br>* Username already used";
					} else if ($_GET['register']==1) {
						echo "<br>* E-mail already used";
					} else {
						echo "";
					}
				} else {
					echo "";
				}
			?>
			</div>
			<form id="register" method="POST" action="register.php" onsubmit="return validateRegister()">
				<div class="field">
					<label class="label" for='fullname'> Full Name </label>
					<input type="text" class="input" id="fullname" name="fullname" placeholder="Fullname">
					<div id="notif-fullname" class="notif">
					</div>
				</div>
				<div class="field">
					<label class="label" for='username'> Username </label>
					<input class="input" type="text" id="username" name="username" placeholder="Username">
					<div id="notif-username" class="notif">
					</div>
				</div>
				<div class="field">
					<label class="label" for='email'> Email </label>				
					<p class="control has-icons-left has-icons-right">
						<span class="icon is-small is-left">
								<i class="fas fa-envelope"></i>
							</span>
						<input class="input" type="text" id="email" name="email" placeholder="example@example.com">
						<div id="notif-email" class="notif">
					</p>
					</div>
				</div>

				<div class="field">
					<label class="label" for='password'> Password </label>			
					<p class="control has-icons-left has-icons-right">
							<span class="icon is-small is-left">
								<i class="fas fa-lock"></i>
							</span>
							<input class="input" type="password" id="password" name="password">
							<div id="notif-password" class="notif"></div>
					</p>
				</div>

				<div class="field">
					<label class="label" for='confirmpassword'> Confirm Password </label>			
					<p class="control has-icons-left has-icons-right">
							<span class="icon is-small is-left">
								<i class="fas fa-lock"></i>
							</span>
							<input class="input" type="password" id="confirmpassword" name="confirmpassword">
							<div id="notif-confirmpassword" class="notif"></div>
					</p>
				</div>

				<div class="field">
					<label class="label" for='address'> Full Address </label>
					<textarea class="textarea" form="register" id="address" name="address"></textarea>
					<div id="notif-address" class="notif">
					</div>
				</div>

				<div class="field">
					<label class="label" for='postalcode'> Postal Code </label>
					<input class="input" type="number" id="postalcode" name="postalcode">
					<div id="notif-postalcode" class="notif">
					</div>			
				</div>

				<div class="field">
					<label class="label" for='phonenumber'> Phone Number </label>
					<input class="input" type="number" id="phonenumber" name="phonenumber">
					<div id="notif-phonenumber" class="notif">
					</div>
				</div>
				<div class="submit-style">
					<input id="button" class="button is-success" type="submit" name="register" value="REGISTER">
				</div>
			</form>
			</div>
		</div>		
	</div>
	</section>
	<section class="section">
		<div class="subtitle has-text-centered"> 
			<b> Already registered? Login <a href="sign-in.php"> here</a> </b>
		</div>
		</section>
	</body>
</html>
