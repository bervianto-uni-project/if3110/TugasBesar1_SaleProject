<?php
	include ('include/connect.php');
	include ('include/functions.php');
	$connect = connectToDatabase();
	$id = $_GET['id_active'];
	$name = getName($connect);
?>

<!DOCTYPE HTML>
<html> 
	<head> 
		<title>Catalog</title>  
    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
		
		<script type="text/javascript" src="js/function.js"></script>
	</head> 

	<header>
		<nav class="navbar" role="navigation" aria-label="main navigation">
			<div class="navbar-brand">
				<a class="navbar-item" href="/">
					
					<h1 class="title"> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
				</a>

				<a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
				</a>
			</div>

			<div id="navbarBasicExample" class="navbar-menu">
				<div class="navbar-start">
					<?php
					echo '<a class="navbar-item is-active" href="catalog.php?id_active='.$id.'">Catalog</a>
						<a class="navbar-item" href="your_products.php?id_active='.$id.'">Your Products</a>
						<a class="navbar-item" href="add_product.php?id_active='.$id.'">Add Products</a>
						<a class="navbar-item" href="sales.php?id_active='.$id.'">Sales</a>
						<a class="navbar-item" href="purchases.php?id_active='.$id.'">Purchases</a>';
					?>
					
				</div>

				<div class="navbar-end">
					<div class="navbar-item">
						Hi, <?php echo $name["username"];?>!
					</div>
					<div class="navbar-item">
						<a href="sign-in.php" class="button is-danger">logout</a>
					</div>
				</div>
			</div>
		</nav>
	</header>

	<body id="body-color"> 
		<section class="section">
		<div class="container">
			<h2 class="title has-text-centered"> What are you going to buy today? </h2>
		<form action="" method="post">
		<div class="field has-addons has-text-centered">
			<div class="control is-expanded">
				<input type="text" name="search" class="input" placeholder="Search catalog..." action="">
			</div>
			<div class="control">
				<input class="button is-primary" type="submit" name="go" value="GO">
			</div>
		</div>
		<div class="field is-horizontal">	
			<div class="field-label is-normal">
				<label class="label">filter by</label>	
			</div>	
			<div class="field-body">
			<div class="control">
				<label class="radio">
				<input type="radio" id="product" name="filter" value="product" checked/> product
				</label>
				<label class="radio">
					<input type="radio" id="store" name="filter" value="store"/> store
				</label>
   		</div>
		 </div>	
		</div>
		<?php
			if (empty($_REQUEST['search'])) {
				$sql = "SELECT * FROM item"; 
				$r_query = mysqli_query($connect,$sql); 
			
			} else {
				$term = mysqli_real_escape_string($connect,$_REQUEST['search']);
				if ($_REQUEST['filter'] == "product") {
					$sql = "SELECT * FROM item WHERE name LIKE '%".$term."%'"; 
					$r_query = mysqli_query($connect,$sql); 
				} else {
					$sql = "SELECT DISTINCT item.id, item.name, item.description, item.price, item.photo, item.seller, item.date, item.time FROM item,account WHERE seller=account.id and account.username LIKE '%".$term."%'"; 
					$r_query = mysqli_query($connect,$sql); 
				}	
			}
			while ($row = mysqli_fetch_assoc($r_query)){  
				
		?>

		<div class="filter">
			
			<div class="product-view">
				<?php 
					$seller = $row['seller'];
					$sqlseller = "SELECT username FROM account WHERE id='$seller'";
					$resultseller = mysqli_query($connect,$sqlseller);
					while($rowseller = mysqli_fetch_assoc($resultseller)) {
						echo $rowseller["username"]; 
					}
				?>
				<br>
				added this on <?php echo $row["date"] ?>, at <?php echo $row["time"]?>
			</div>
			<div class="product-view">
				<div class="photo">
					<img src=<?php echo $row["photo"]; ?> alt="Mountain View" width="100px" height="100px">
				</div>
				<div class="description">
					<b><?php echo $row["name"] ?></b> <br>
					<?php echo "IDR ".showPrice($row["price"]) ?><br>
					<font size="1"> <?php echo $row["description"] ?></font>
				</div>
				<?php echo '<div class="detail" id="detail'.$row['id'].'">'; ?>
					
					<div>
						<font size="1"> <?php echo likes($connect,$row['id']) ?> likes </font>
					</div>
					<div class="npurchase">
						<font size="1"> <?php echo purchases($connect,$row['id']) ?> purchases </font><br>
					</div>
					<?php 
						if (check_ip($connect,$row['id'],$id) == 0) { 
							echo '<div class="like-button">
								<b><a onClick="getLike('.$id.','.$row['id'].');return false;" href="" class="like" >LIKE								
								</a></b>
								</div>';
						} else {
							echo'<div class="like-button">
								<b><a href="" onclick="getDislike('.$id.','.$row['id'].');return false;" class="liked" >LIKED
								</a></b>
								</div>';
						}
					?>
					<div class="buy-button">
						<?php
						echo ('<b><a href="confirm_purchase.php?id_active='.$id.'&id_item='.$row['id'].'" style="text-decoration:none;"><font color="#5f793e" size="2">&nbsp;&nbsp;BUY</font></a></b>')
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
			}
		?>
		</form>	
		</div>
		</section>
	</body> 
</html>